import * as constants from "../actionsConstants";

/**Login screen ****/
export const authenticating = () => {
    return {
        type: constants.AUTHENTICATING
    };
};
export const authenticated = (user) => {
    return {
        type: constants.AUTHENTICATED,
        user: user
    };
};
export const login = (username, password) => {
    return (dispatch) => {
        dispatch(authenticating())
        setTimeout(() => {
            const user = { id: 123, name: 'Bharat Chavan' };
            dispatch(authenticated(user))
        }, 3000);
    };
}

export const logout = () => {
    return {
        type: constants.LOGOUT
    };
};

/**Registration  screen ****/
export const registeringUser = (username, password) => {
    return {
        type: constants.REGISTERING_USER
    };
}
export const userRegistered = (user) => {
    return {
        type: constants.USER_REGISTERED,
        user: user
    }
}
export const signup = (username, password) => {
    return (dispatch) => {
        dispatch(registeringUser(username, password))
        setTimeout(() => {
            const user = { id: 123, name: 'Bharat Chavan' };
            dispatch(userRegistered(user))
        }, 3000);
    };
};