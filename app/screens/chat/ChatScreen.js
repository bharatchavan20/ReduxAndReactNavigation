import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
export default class ChatScreen extends Component {
    constructor(props) {
        super(props)
        this.onPress = this.onPress.bind(this);
    }


    onPress = () => {
        try {
            this.props.navigation.navigate("");
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        return (
            <View >
                <Text >This is ChatScreen</Text>
                <Button title="GO to " onPress={this.onPress} />
            </View>
        );
    }
}