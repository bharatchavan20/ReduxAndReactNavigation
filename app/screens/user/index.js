import ProfileDetailsScreen from './ProfileDetailsScreen';
import ProfileUpdateScreen from './ProfileUpdateScreen';


module.exports = {
    ProfileDetailsScreen, ProfileUpdateScreen
}