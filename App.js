import React, { Component } from 'react';
import { RootStack } from "./app/Navigator";



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      initializationInProgress: true,
      isLoading: true
    }
    this.redirect = this.redirect.bind(this);
  }

  redirect = () => {
    this.setState({ initializationInProgress: false });
  }
  componentDidMount() {
    setTimeout(this.redirect, 0)
  }
  render() {
    return (<RootStack />);
  }
}

export default App;

