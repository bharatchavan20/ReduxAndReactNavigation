import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button
} from 'react-native';
export default class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.onPress = this.onPress.bind(this);
    }


    onPress = () => {
        try {
            this.props.navigation.navigate("profile");
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        return (
            <View >
                <Text >This is Dashboard</Text>
                <Button title="GO to " onPress={this.onPress} />
            </View>
        );
    }
}