import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button
} from 'react-native';
export default class ProfileDetailsScreen extends Component {
    constructor(props) {
        super(props)
        this.onPress = this.onPress.bind(this);
    }


    onPress = () => {
        try {
            this.props.navigation.navigate("search");
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        return (
            <View >
                <Text >This is a ProfileDetailsScreen</Text>
                <Button title="GO to " onPress={this.onPress} />
            </View>
        );
    }
}