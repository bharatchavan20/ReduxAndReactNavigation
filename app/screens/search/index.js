import SearchResult from './SearchResult';
import SearchScreen from './SearchScreen';

module.exports = {
    SearchResult, SearchScreen
}