import LoginScreen from './LoginScreen';
import RegistrationScreen from './RegistrationScreen';
import ResetPasswordScreen from './ResetPasswordScreen';


module.exports = {
    LoginScreen, RegistrationScreen, ResetPasswordScreen
}