import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button
} from 'react-native';
import { NavigationActions } from 'react-navigation';
export default class ResetPasswordScreen extends Component {
    constructor(props) {
        super(props)
        this.onPress = this.onPress.bind(this);
        this.naviagateToDashboard = this.naviagateToDashboard.bind(this);
    }


    onPress = () => {
        try {
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'dashboard' })],
            });
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
            console.log(error)
        }
    }

    naviagateToDashboard = () => {
        try {
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'dashboard' })],
            });
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        return (
            <View >
                <Text >This is a ResetPasswordScreen</Text>
                <Button title="GO to " onPress={this.onPress} />
            </View>
        );
    }
}