import React, { Component } from 'react';
import { AppRegistry} from 'react-native';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
]);
import { Provider } from 'react-redux';
import App from './App';
import store from './app/redux/store';




export default class AppEntry extends Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('PerfectNavigation', () => AppEntry);
