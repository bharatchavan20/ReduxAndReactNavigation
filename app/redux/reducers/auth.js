import * as constants from "../actionsConstants";

const defaultState = {
    isLoggedIn: false,
    authenticating: false,
    authenticated: false,
    registeringUser: false,
    userRegistered: false,
    user: {
        id: undefined,
        name: null,
        avatar: 'https://abs.twimg.com/sticky/default_profile_images/default_profile_3_400x400.png',
    }
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case constants.AUTHENTICATING:
            return Object.assign({}, state, {
                authenticating: true,
            });
        case constants.AUTHENTICATED:
            return Object.assign({}, state, {
                isLoggedIn: true,
                authenticating: false,
                authenticated: true,
                user: action.user,

            });
        case constants.LOGOUT:
            return Object.assign({}, state, defaultState);
        case constants.REGISTERING_USER:
            return Object.assign({}, state, {
                registeringUser: true,
            });
        case constants.USER_REGISTERED:
            return Object.assign({}, state, {
                userRegistered: true,
                registeringUser: false,
                isLoggedIn:true,
                user: action.user
            });
        default:
            return state;
    }
}