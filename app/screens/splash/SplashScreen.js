import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button, ActivityIndicator
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';

class SplashScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            initializationInProgress: true,
            isLoading: true
        }
        this.redirect = this.redirect.bind(this);
    }

    redirect = () => {
        let resetAction;
        try {
            if (this.props.isLoggedIn) {
                resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
                });

            } else {
                resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Login' })],
                });
            }
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
            console.log(error)
            alert(error);
        }

    }
    componentDidMount() {
        setTimeout(this.redirect, 0)
    }

    onPress = () => {
        try {
            this.props.navigation.navigate("");
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    };
}

export default connect(mapStateToProps)(SplashScreen);