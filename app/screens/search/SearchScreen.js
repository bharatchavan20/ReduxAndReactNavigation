import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button, BackHandler
} from 'react-native';
import { NavigationActions, HeaderBackButton } from 'react-navigation';
export default class SearchScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
        return {
            title: "Search screen",
            headerBackTitle: null,
            headerLeft: <HeaderBackButton onPress={params.onBackPress} />
        }
    };
    constructor(props) {
        super(props)
        this.onPress = this.onPress.bind(this);
        this.naviagateToDashboard = this.naviagateToDashboard.bind(this);
        this.onHardwareBackPress = this.onHardwareBackPress.bind(this);
    }
    componentWillMount() {
        this.props.navigation.setParams({ onBackPress: this.naviagateToDashboard });
        BackHandler.addEventListener('hardwareBackPress', this.onHardwareBackPress);
    }
    componentDidMount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }


    onPress = () => {
        try {
            this.props.navigation.navigate("");
        } catch (error) {
            console.log(error)
        }
    }
    onHardwareBackPress = () => {
        this.naviagateToDashboard();
        return true;
    }
    naviagateToDashboard = () => {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'dashboard' })],
        });
        this.props.navigation.dispatch(resetAction);
    }
    render() {
        return (
            <View >
                <Text >This is SearchScreen</Text>
                <Button title="GO to " onPress={this.onPress} />
            </View>
        );
    }
}