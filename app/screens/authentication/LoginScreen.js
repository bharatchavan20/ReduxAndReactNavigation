import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { FormInput, Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { login, signup } from '../../redux/actions/auth';
class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            message: ''
        };
        this.naviagateToDashboard = this.naviagateToDashboard.bind(this);
        this.naviagateToRegistration = this.naviagateToRegistration.bind(this);
        this.onLoginPress = this.onLoginPress.bind(this);
    }

    onLoginPress = () => {
        this.props.onLogin(this.state.username, this.state.password);
    }
    naviagateToDashboard = () => {
        try {
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
            });
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
            console.log(error)
        }
    }
    naviagateToRegistration = () => {
        try {
            this.props.navigation.navigate('Registration');
        } catch (error) {
            console.log(error)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.authenticating) {
            this.setState({ message: "inprogress" });
        } else if (nextProps.authenticated) {
            this.setState({ message: "authenticated" });
            this.naviagateToDashboard()
        }
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                    <FormInput
                        placeholder='Username'
                        autoCapitalize='none'
                        autoCorrect={false}
                        autoFocus={true}
                        keyboardType='email-address'
                        value={this.state.username}
                        onChangeText={(text) => this.setState({ username: text })} />
                    <FormInput
                        placeholder='Password'
                        autoCapitalize='none'
                        autoCorrect={false}
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(text) => this.setState({ password: text })} />
                    <View style={{flexDirection: 'row' }}>
                        <Button
                            raised
                            title='Login'
                            onPress={this.onLoginPress} />
                        <Button
                            raised
                            title='SignUp'
                            onPress={this.naviagateToRegistration} />
                    </View>
                    <Text style={{ margin: 10, fontSize: 16, fontWeight: 'bold' }}>{this.state.message}</Text>
                </View>

            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        authenticating: state.auth.authenticating,
        authenticated: state.auth.authenticated
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (username, password) => { dispatch(login(username, password)); },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);