import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FormInput, Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import { signup } from '../../redux/actions/auth';
import { connect } from 'react-redux';
class RegistrationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            message: ''
        };
        this.onRegisterPress = this.onRegisterPress.bind(this)
        this.naviagateToDashboard = this.naviagateToDashboard.bind(this);
    }
    onRegisterPress = () => {
        this.props.onSignup(this.state.username, this.state.password);
    }

    naviagateToDashboard = () => {
        try {
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
            });
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
            console.log(error)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.registeringUser) {
            this.setState({ message: "Registering..." });
        } else if (nextProps.userRegistered) {
            this.setState({ message: "authenticated" });
            this.naviagateToDashboard()
        }
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                    <FormInput
                        placeholder='Username'
                        autoCapitalize='none'
                        autoCorrect={false}
                        autoFocus={true}
                        keyboardType='email-address'
                        value={this.state.username}
                        onChangeText={(text) => this.setState({ username: text })} />
                    <FormInput
                        placeholder='Password'
                        autoCapitalize='none'
                        autoCorrect={false}
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(text) => this.setState({ password: text })} />
                    <Button
                        raised
                        title='Register'
                        onPress={this.onRegisterPress} />

                    <Text style={{ margin: 10, fontSize: 16, fontWeight: 'bold' }}>{this.state.message}</Text>
                </View>

            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        registeringUser: state.auth.registeringUser,
        userRegistered: state.auth.userRegistered
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSignup: (username, password) => { dispatch(signup(username, password)); },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen);