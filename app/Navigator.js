import { StackNavigator } from "react-navigation";
import { ProfileDetailsScreen, ProfileUpdateScreen } from './screens/user';
import { NotificationsScreen } from './screens/notifications';
import { SearchResult, SearchScreen } from './screens/search';
import { LoginScreen, RegistrationScreen, ResetPasswordScreen } from './screens/authentication/';
import Dashboard from './screens/Dashboard';
import SplashScreen from "./screens/splash/SplashScreen";



const AuthenicationStack = StackNavigator({
    Login: { screen: LoginScreen },
    Registration: { screen: RegistrationScreen },
    ResetPassword: { screen: ResetPasswordScreen }
})

const ProfileStack = StackNavigator({
    ProfileDetails: { screen: ProfileDetailsScreen },
    ProfileUpdate: { screen: ProfileUpdateScreen },
})
const SearchStack = StackNavigator({
    SearchScreen: { screen: SearchScreen },
    SearchResult: { screen: SearchResult },
})
const DashboardStack = StackNavigator({
    dashboard: { screen: Dashboard }
});


export const RootStack = StackNavigator({
    Splash: {
        screen:SplashScreen
    },
    Login: {
        screen: LoginScreen
    },
    Registration: {
        screen: RegistrationScreen
    },
    Dashboard: {
        screen: Dashboard
    },
    Prfile: {
        screen: ProfileStack
    },
    Search: {
        screen: SearchStack
    },
    Notifications: {
        screen: NotificationsScreen
    }
}, { headerMode: 'none', mode: 'modal', });